import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../../../services/noticias.service';
import { ActivatedRoute } from '@angular/router';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-edit-create',
  templateUrl: './edit-create.component.html',
  styleUrls: ['./edit-create.component.css']
})
export class EditCreateComponent implements OnInit {
  public baseUrl = environment.apiRoot;

  public uploader: FileUploader = new FileUploader({url: this.baseUrl + 'uploadImg', itemAlias: 'imgUrl'});

  showPreview = false;
  noticiaForm: FormGroup;
  editMode: boolean;
  contenidoForm: string;
  public noticia: {
    completa: string,
    vistaPrevia: {
      titulo: string,
      contenido: string,
      imgUrl: string
    }
  };
  id: number;
  constructor(private noticiasService: NoticiasService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('ImageUpload:uploaded:', item, status, response);
         response = response.replace(/"public/, this.baseUrl + 'public');
         response = response.replace(/"/g, '');
         response = response.replace(/\\\\/g, '/');
         this.noticia = {
          completa: this.noticia.completa,
          vistaPrevia: {
            titulo: this.noticiaForm.value['titulo'],
            contenido: this.noticiaForm.value['contenido'],
            imgUrl: response,
          }
        };
        if (!this.editMode) {
          // this.noticiasService.postNoticia(this.noticia).subscribe(result => console.log(result));
          this.noticiasService.postNoticia(this.noticia).subscribe( function (result) {
            console.log(result);
          });
          this.showPreview = true;
        } else {
          this.noticiasService.editNoticia(this.id, this.noticia).subscribe(result => console.log(result), err => console.log(err));
          this.showPreview = true;
        }
     };
    this.noticia = {
      completa: '',
      vistaPrevia: {
        titulo: '', contenido: '', imgUrl: ''
      }
    };
    this.initForm();
    this.editMode = false;
    this.route.params.subscribe(params => {
      console.log(params);
      this.id = params['id'];
      if (this.id != null) {
        this.editMode = true;
        this.noticiasService.getNoticia(this.id).subscribe(noticias => {
          this.noticia = noticias.articulo;
          this.contenidoForm = this.noticia.vistaPrevia.contenido.replace(/<br>/g, '\n');
          this.noticia.vistaPrevia.contenido = this.noticia.vistaPrevia.contenido.replace(/\n/g, '<br>');
          console.log(this.noticia);
          this.initForm();
        });
      }
    });
  }

  onSubmit() {
    this.uploader.uploadAll();
  }

  private initForm() {
    let noticiaName = '';
    let noticiaDescription = '';
    const noticiaImagePath = '';
    if (this.editMode) {
      noticiaName = this.noticia.vistaPrevia.titulo;
      noticiaDescription = this.contenidoForm;
    }
    this.noticiaForm = new FormGroup({
      titulo: new FormControl(noticiaName, Validators.required),
      imgUrl: new FormControl(noticiaImagePath, Validators.required),
      contenido: new FormControl(noticiaDescription, Validators.required)
    });
  }
}
