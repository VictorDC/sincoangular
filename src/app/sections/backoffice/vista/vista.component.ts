import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NoticiasService } from '../../../services/noticias.service';

@Component({
  selector: 'app-vista',
  templateUrl: './vista.component.html',
  styleUrls: ['./vista.component.css']
})
export class VistaComponent implements OnInit {
  public noticias: any;

  constructor( public noticiasService: NoticiasService,
              public router: Router) {
  }

  ngOnInit() {

    this.noticiasService.getNoticias().subscribe(noticias => {
      this.noticias = noticias.articulos;
      this.noticias.sort(function (a, b) {
        if (a.updatedAt > b.updatedAt) {
          return -1;
        }
        if (a.updatedAt < b.updatedAt) {
          return 1;
        }
        return 0;
      });
      console.log(this.noticias);
    });
  }

  onEdit(id) {
    this.router.navigate(['/noticias/admin/editar/' + id]);
  }

  onDelete(id) {
    this.noticiasService.deleteNoticia(id).subscribe(result => {
      console.log(result);
      this.noticiasService.getNoticias().subscribe(noticias => {
        this.noticias = noticias.articulos;
        console.log(this.noticias);
      });
    });
  }

}
