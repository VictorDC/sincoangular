import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ApiService } from "../../services/api.service";
import { UserService } from "../../services/user.service";
import { LoginModel } from "../../models/login.model";

import { StringsConstant } from "../../constants/strings.constant";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl: string;
  fromRegister: boolean;
  fromRegisterMessage: string;
  errorMessage: string;
  temporaryPassMessage: string;
  inPasswordStep: boolean;
  isPreregistered: boolean;

  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private user: UserService
  ) {}

  ngOnInit() {
    this.isPreregistered = false;
    this.inPasswordStep = false;
    this.loginForm = this.createFormGroup(this.formBuilder);
    this.fromRegisterMessage = StringsConstant.SUCCESSFUL_REGISTER;
    if (this.user.isLogged()) {
      this.router.navigate(["/admin/dashboard"]);
    } else {
      // This isn't mandatory since error.interceptor helper cleans localstorage
      // data when 401 HTTP Errors are raised from API. We do it nonetheless.
      this.user.logout();
      this.returnUrl =
        this.route.snapshot.queryParams["returnUrl"] || "/admin/dashboard";
      this.fromRegister = this.route.snapshot.queryParams.hasOwnProperty(
        "fromRegister"
      );
    }
  }

  /**
   * Triggers chain of SINCO API request to perform registration
   */
  onSubmit() {
    /**
     * Performs registration request if appToken exists in local storage.
     * Performs app credentials request otherwise.
     */
    this.requestLogin();
  }

  /**
   * Performs login request to SINCO API from form values.
   * Sets credentials and user's profile if success.
   */
  requestLogin(): void {
    const result: LoginModel = Object.assign({}, this.loginForm.value);
    this.api.login(result).subscribe(
      data => {
        const accessToken = data.accessToken;
        const username = result.username;
        this.user.setCredentials(username, accessToken);
        this.requestProfile();
      },
      error => {
        if (error.status === 401) {
          this.errorMessage = StringsConstant.INVALID_CREDENTIALS;
        } else {
          this.errorMessage = StringsConstant.UNKNOWN_ERROR;
        }
        // this.loaderService.showLoader(false);
      }
    );
  }

  /**
   * Performs app credentials request to SINCO API.
   * Sets app token and request email status when success.
   */
  requestAppCredentials(): void {
    // this.loaderService.showLoader(true);
    this.api.appAccessToken().subscribe(
      data => {
        const appToken = data.access_token;
        this.user.setAppToken(appToken);
      },
      () => {
        this.errorMessage = StringsConstant.UNKNOWN_ERROR;
        // this.loaderService.showLoader(false);
      }
    );
  }

  /**
   * Resets login form and goes to email step.
   */
  clearEmail() {
    this.loginForm.reset();
    this.inPasswordStep = false;
  }

  /**
   * Creates login form from a LoginModel instance and sets
   * its validators functions.
   * @param formBuilder
   */
  private createFormGroup(formBuilder: FormBuilder) {
    // tslint:disable-next-line:max-line-length
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let formGroup: FormGroup;
    formGroup = formBuilder.group(new LoginModel());
    formGroup.controls["username"].setValidators([
      Validators.required,
      Validators.pattern(emailRegex)
    ]);
    formGroup.controls["password"].setValidators([Validators.required]);
    return formGroup;
  }

  /**
   * Performs profile request to SINCO API.
   * Sets the profile and navigates to return URL if the current
   * user has a previously setted password. Navigates to set-password
   * section otherwise.
   */
  private requestProfile(): void {
    this.api.getProfile().subscribe(
      data => {
        this.user.setProfile(data);
        // this.loaderService.showLoader(false);
        this.router.navigate([this.returnUrl]);
      },
      () => {
        this.errorMessage = StringsConstant.UNKNOWN_ERROR;
        // this.loaderService.showLoader(false);
      }
    );
  }
}
