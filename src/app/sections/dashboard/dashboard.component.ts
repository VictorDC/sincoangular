import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
// import {MatFormFieldModule} from '@angular/material/form-field';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  // @Input()  //Nombre
  // @Input()  //Apellido
  // @Input()  //Correo
  // @Input()  //Organización
  // @Input()  //mensaje
  @HostListener('window:resize', ['$event'])
  public innerWidth: any;
  testimonio: boolean;

  ngOnInit() {
    if(window.innerWidth > 1699){
      this.testimonio=true;
    } else {this.testimonio = false}
  }

  onResize(event) {
    if (event.target.innerWidth > 1699) { this.testimonio = true;
    } else { this.testimonio = false; }
    console.log(this.testimonio)
    this.innerWidth=window.innerWidth;
    console.log(this.innerWidth)

  }

  onEnviar(){

  }
}
