import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../../../../services/noticias.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  public noticias: any;

  public pagina = 1;


  constructor( public noticiasService: NoticiasService,
    public router: Router) {
}
  ngOnInit() {
    this.noticiasService.getNoticias().subscribe(noticias => {
      this.noticias = noticias.articulos;
      this.noticias.sort(function (a, b) {
        if (a.updatedAt > b.updatedAt) {
          return -1;
        }
        if (a.updatedAt < b.updatedAt) {
          return 1;
        }
        return 0;
      });
      this.noticias.forEach(noticia => {
        noticia.fecha = moment(noticia.updatedAt).locale('es').format('LL');
        console.log(this.noticias);
      });
    });
  }

  verArticulo(id) {
    this.router.navigate(['/noticias/blog/articulo/' + id]);
  }

}
