import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NoticiasService } from '../../../../services/noticias.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {

  constructor(private noticiasService: NoticiasService,
    private route: ActivatedRoute) { }

  public noticia: {
    completa: string,
    vistaPrevia: {
      titulo: string,
      contenido: string,
      imgUrl: string
    }
  };

  public id = null;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
        this.noticiasService.getNoticia(this.id).subscribe(noticias => {
          this.noticia = noticias.articulo;
        });
    });
  }

}
