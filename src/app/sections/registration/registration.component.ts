import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

// Service
import {ApiService} from '../../services/api.service';
import {UserService} from '../../services/user.service';

import { StringsConstant } from '../../constants/strings.constant';
import { UserRegistrationModel } from '../../models/user-registration.model';

import { CustomErrorStateMatcher } from 'src/app/utils/custom-error-state-matcher';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  errorMessage: string;
  returnUrl: string;
  existingEmail: boolean;
  matcher = new CustomErrorStateMatcher();

  constructor(private api: ApiService,
    private user: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) {}

    get f() { return this.registrationForm.controls; }

  ngOnInit() {
    this.existingEmail = false;

    if (this.user.isLogged()) {
      this.router.navigate(['/admin/dashboard']);
    } else {
      // This isn't mandatory since error.interceptor helper cleans localstorage
      // data when 401 HTTP Errors are raised from API. We do it nonetheless.
      this.user.logout();
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/admin/dashboard';
    }

        /**
     * Form creation and class variables initialization
     */
    this.registrationForm = this.formBuilder.group(
      new UserRegistrationModel,
      {
        validator: [
          this.mismatchingPasswords
        ]
      }
    );
    this.setFormControlsValidators();
  }


/**
   * Triggers chain of SINCO API request to perform registration
   */
  onSubmit() {
    this.cleanErrors();
    /**
     * Performs registration request if appToken exists in local storage.
     * Performs app credentials request otherwise.
     */
    if (localStorage.getItem('appToken')) {
      this.requestRegister();
    } else {
      this.requestAppCredentials();
    }
  }

  /**
   * Class' form validator: checks if both new password and its confirmation
   * are the same.
   * @param registrationForm - Registration form instance
   * @returns Object - null if passwords match, 'missmatching-pass' error otherwise.
   */
  private mismatchingPasswords(registrationForm: FormGroup) {
    const pass1 = registrationForm.controls.password.value;
    const pass2 = registrationForm.controls.password2.value;

    return pass1 === pass2 ? null : { 'missmatching-pass': true };
  }


    /**
   * Sets form controls validator functions
   */
  private setFormControlsValidators() {
    const nameRegex = /^[a-zA-Z,\.'\-]+$/;
    // tslint:disable-next-line:max-line-length
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    Object.keys(this.registrationForm.controls).forEach(key => {
      /* We mark all form fields as required since all of them are required :) */
      const validators = [Validators.required];
      if (key === 'email') {
        validators.push(Validators.pattern(emailRegex));
      } else if (key === 'firstName' || key === 'lastName') {
        validators.push(Validators.pattern(nameRegex));
      } else if (key === 'password') {
        validators.push(Validators.minLength(8));
      }
      this.registrationForm.get(key).setValidators(validators);
    });
  }

  /**
   * Performs app credentials request to SINCO API.
   * Sets the app token and performs registration request when success.
   */
  private requestAppCredentials(): void {
    // this.loaderService.showLoader(true);
    this.api.appAccessToken()
      .subscribe(
        data => {
          const appToken = data.accessToken;
          this.user.setAppToken(appToken);
          this.requestRegister();
          console.log(appToken);
        },
        error => {
          if (error.status === 422) {
            console.log(error);
            const messageObject = error.error.errors;
            this.existingEmail = messageObject.hasOwnProperty('email');
          } else {
            this.errorMessage = StringsConstant.UNKNOWN_ERROR;
          }
          // this.loaderService.showLoader(false);
        }
      );
  }

  /**
   * Creates request body object from registrarion form instance
   * @returns requestBody - Object with the required data to user registration request
   */
  private getRequestBody(): Object {
    const result: UserRegistrationModel = Object.assign({}, this.registrationForm.value);
    const requestBody: Object = {
      name: result.firstName,
      lastName: result.lastName,
      email: result.email,
      password: result.password,
      role: 'user'
    };

    return requestBody;
  }

  /**
   * Performs user registration request to SINCO API.
   * Redirects to Login Section if succeeds, shows the appropriate
   * error message otherwise.
   */
  private requestRegister(): void {
    const requestBody = this.getRequestBody();
    this.api.register(requestBody)
      .subscribe(
        data => {
          this.router.navigate(['/login'], { queryParams: { fromRegister: 'true' } });
          console.log('Registrado', data);
          // this.loaderService.showLoader(false);
        },
        error => {
          if (error.status === 422) {
            console.log(error);
            const messageObject = error.error.errors;

          } else {
            this.errorMessage = StringsConstant.UNKNOWN_ERROR;
          }
          // this.loaderService.showLoader(false);
        }
      );
  }

  /**
   * Makes sure to hide any error message that could be shown to the user.
   */
  private cleanErrors(): void {
    this.errorMessage = undefined;
  }

}

