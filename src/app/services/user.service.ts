import { Injectable } from '@angular/core';

import { CurrentUser } from '../interfaces/current-user.interface';
import { UserProfile } from '../interfaces/user-profile.interface';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor() { }

  /**
   * Sets app token in local storage
   * @param appToken - Application token
   */
  setAppToken(appToken: string): void {
    localStorage.setItem('appToken', appToken);
  }

  /**
   * Sets users credentials in local storage
   * @param username - Username
   * @param accessToken - User Access Token
   */
  setCredentials(username: string, accessToken: string): void {
    localStorage.setItem('currentUser', JSON.stringify({
      username: username,
      token: accessToken
    }));
  }

  /**
   * Checks if current user is logged.
   * @returns - Boolean: whether current user is logged or not
   */
  isLogged(): boolean {
    return localStorage.getItem('currentUser') !== null;
  }

  /**
   * Sets current user profile
   * @param profile - UserProfile: object with user data
   */
  setProfile(profile: UserProfile): void {
    const currentUser = this.getCurrentUser();
    currentUser.profile = profile;
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
  }

  /**
   * Gets current user's profile.
   * @returns - UserProfile: object with current user data
   */
  getProfile(): UserProfile {
    const currentUser = this.getCurrentUser();
    return currentUser.profile;
  }

  /**
   * Gets current user data
   * @returns - Object with current user data
   */
  getCurrentUser(): CurrentUser {
    return JSON.parse(localStorage.getItem('currentUser'));
  }


  /**
   * Performs user logout
   */
  logout(): void {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('appToken');
  }

}
