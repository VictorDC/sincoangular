import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

// Angular takes care of swapping the environment file for the correct one.
import { environment } from '../../environments/environment';
import { LoginModel } from '../models/login.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private API_ROOT = environment.apiRoot;

  private getEndpointURL(endpointURL: string): string {
    return this.API_ROOT + endpointURL;
  }

  /**
   * POST: performs authentication with the API
   * @param loginModel - LoginModel class (username and password object)
   */
  login(loginModel: LoginModel): Observable<any> {
    const loginOptions = {
      'grant_type': 'password',
      'client_id': 'confidentialApplication',
      'client_secret': 'topSecret'
    };
    const endpointURL = this.getEndpointURL('oauth/token');
    const body = Object.assign(loginOptions, loginModel);
    console.log(body);
    return this.http.post<any>(endpointURL, body, httpOptions);
  }

  /**
   * POST: performs app authentication with the API
   */
  appAccessToken(): Observable<any> {
    const appTokenOptions = {
      'grant_type': 'client_credentials',
      'client_id': 'confidentialApplication',
      'client_secret': 'topSecret'
    };
    const endpointURL = this.getEndpointURL('oauth/token');
    return this.http.post<any>(endpointURL, appTokenOptions, httpOptions);
  }

  /**
   * POST: performs authentication with the API
   * @param userRegisterBody - Object with new user's necessary data
   */
  register(userRegisterBody: Object): Observable<any> {
    const endpointURL = this.getEndpointURL('user/register');
    const appToken = 'Bearer ' + localStorage.getItem('appToken');
    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': appToken
      })
    };
    return this.http.post<any>(endpointURL, userRegisterBody, options);
  }

  /**
   * GET: gets profile of current user
   */
  getProfile(): Observable<any> {
    const endpointURL = this.getEndpointURL('user/profile');
    return this.http.get<any>(endpointURL, httpOptions);
  }

  /** USER ENDPOINTS
   * GET: gets all the users
   */

   getUsers(): Observable<any> {
    const endpointURL = this.getEndpointURL('users');
    const appToken = 'Bearer ' + localStorage.getItem('appToken');
    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': appToken
      })
    };
    return this.http.get<any>(endpointURL, options);
  }

  constructor(
    private http: HttpClient
  ) { }

}
