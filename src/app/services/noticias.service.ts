import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  private url = environment.apiRoot;
  constructor(private http: HttpClient) {

  }
  getNoticias(): Observable<any> {
    return this.http.get(this.url + 'articulos');
  }
  getNoticia(id): Observable<any> {
    return this.http.get(this.url + 'articulo/' + id);
  }
  postNoticia(noticia): Observable<any> {
    return this.http.post(this.url + 'articulo', noticia);
  }
  editNoticia(id, noticia): Observable<any> {
    return this.http.put(this.url + 'articulo/' + id, noticia);
  }
  deleteNoticia(id): Observable<any> {
    return this.http.delete(this.url + 'articulo/' + id);
  }
}
