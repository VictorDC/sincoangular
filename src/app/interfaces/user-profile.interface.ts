export interface UserProfile {
  id: number;
  name: string;
  email: string;
  role: string;
  phone: string;
  created_at: string;
  updated_at: string;
}
