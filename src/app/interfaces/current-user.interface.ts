import { UserProfile } from './user-profile.interface';

export interface CurrentUser {
  profile: UserProfile;
  token: string;
  username: string;
}
