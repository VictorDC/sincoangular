import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EditorModule } from '@tinymce/tinymce-angular';
import { FroalaViewModule } from 'angular-froala-wysiwyg';

import { FileUploadModule } from 'ng2-file-upload';

import {NgxPaginationModule} from 'ngx-pagination';

// Angular Material Modules
import { AngularMaterialModule } from './material/angularmaterial.module';

// Routing handling library
import { AppRoutingModule } from './app.routes';

// Http request handling library
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Helpers
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { AuthInterceptor } from './_helpers/auth.interceptor';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './sections/login/login.component';
import { FooterComponent } from './components/footer/footer.component';
import { BlogComponent } from './sections/noticias/blog/blog.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { NoticiasComponent } from './sections/noticias/noticias.component';
import { ContactoComponent } from './sections/contacto/contacto.component';
import { AcademiaComponent } from './sections/academia/academia.component';
import { VistaComponent } from './sections/backoffice/vista/vista.component';
import { DashboardComponent } from './sections/dashboard/dashboard.component';
import { BackofficeComponent } from './sections/backoffice/backoffice.component';
import { ListadoComponent } from './sections/noticias/blog/listado/listado.component';
import { RegistrationComponent } from './sections/registration/registration.component';
import { ArticuloComponent } from './sections/noticias/blog/articulo/articulo.component';
import { EditCreateComponent } from './sections/backoffice/edit-create/edit-create.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    BlogComponent,
    VistaComponent,
    LoginComponent,
    NavBarComponent,
    FooterComponent,
    ListadoComponent,
    ContactoComponent,
    AcademiaComponent,
    NoticiasComponent,
    ArticuloComponent,
    DashboardComponent,
    BackofficeComponent,
    EditCreateComponent,
    RegistrationComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule,
    FroalaViewModule.forRoot(),
    FileUploadModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    AngularMaterialModule
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
