export class StringsConstant {
  public static UNKNOWN_ERROR = 'Ha ocurrido un error inesperado, intente de nuevo más tarde';
  public static UNKNOWN_EMAIL = 'El correo que ha especificado no está registrado en PANA';
  public static INVALID_EMAIL = 'Por favor, introduce un correo válido';
  public static INVALID_CREDENTIALS = 'La combinación usuario y contraseña no es válida';
  public static RESET_PASS = 'Hemos enviado los pasos para restablecer su contraseña al correo especificado';
  public static SUCCESSFUL_REGISTER = 'Tu registro se ha realizado exitosamente. Hemos enviado los pasos para la verificación a tu correo electrónico';
  public static BLOCKED_USER = 'Eror: su cuenta se encuentra bloqueada';
}