import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// Guard
import { AuthGuard } from './_guards/auth.guard';

// Components
import { DashboardComponent } from './sections/dashboard/dashboard.component';
import { NoticiasComponent } from './sections/noticias/noticias.component';
import { BlogComponent } from './sections/noticias/blog/blog.component';
import { BackofficeComponent } from './sections/backoffice/backoffice.component';
import { ContactoComponent } from './sections/contacto/contacto.component';
import { AcademiaComponent } from './sections/academia/academia.component';
import { EditCreateComponent } from './sections/backoffice/edit-create/edit-create.component';
import { VistaComponent } from './sections/backoffice/vista/vista.component';
import { ListadoComponent } from './sections/noticias/blog/listado/listado.component';
import { ArticuloComponent } from './sections/noticias/blog/articulo/articulo.component';
import { LoginComponent } from './sections/login/login.component';
import { RegistrationComponent } from './sections/registration/registration.component';

const ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrationComponent },
    {path: 'noticias', component: NoticiasComponent, children: [
      {path: 'blog', component: BlogComponent, children: [
        {path: 'articulo/:id', component: ArticuloComponent },
        {path: '', component: ListadoComponent }
      ] },
      {path: 'admin', component: BackofficeComponent, children: [
        {path: 'crear', component: EditCreateComponent },
        {path: 'editar/:id', component: EditCreateComponent },
        {path: '', component: VistaComponent }
      ] }
    ] },
    {path: 'contacto', component: ContactoComponent },
    {path: 'academia', component: AcademiaComponent },
    {path: 'inicio', component: DashboardComponent },
    {path: '', redirectTo: '/inicio', pathMatch: 'full'},
    // {path: '**', redirectTo: '/inicio' }
  ];

@NgModule({
    imports: [RouterModule.forRoot(ROUTES)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
